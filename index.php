<?php

require('animal.php');
require('frog.php');
require('ape.php');

echo "<h1>Latihan OOP PHP</h1>";

$sheep = new Animal("Shaun");

echo "Name: " . $sheep->name . "<br>";
echo "Legs: " . $sheep->legs . "<br>";
echo "Cold blooded: " . $sheep->cold_blooded . "<br><br>";

// Frog
$kodok = new Frog("buduk");

echo "Name: " . $kodok->name . "<br>";
echo "Legs: " . $kodok->legs . "<br>";
echo "Cold blooded: " . $kodok->cold_blooded . "<br>";
echo "Jump: " . $kodok->jump() . "<br><br>";

// Ape
$sungokong = new Ape("kera sakti");

echo "Name: " . $sungokong->name . "<br>";
echo "Legs: " . $sungokong->legs . "<br>";
echo "Cold blooded: " . $sungokong->cold_blooded . "<br>";
echo "Yell: " . $sungokong->yell();

?>